from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from time import sleep
from pprint import pprint
from enum import Enum

import json
import requests

class Signal(Enum):
    SIG_AWAKEN = 100
    SIG_SLEEP = 200
    SIG_DISCONNECT = 300

class MQTTProtocolHandler(AWSIoTMQTTClient):

    def __init__(self, client_id):
        self.endpoint = ''
        self.clientID = client_id
        self.port = 443

        with open('config.json') as aws_config:
            config = json.loads(aws_config.read())
            self.endpoint = config['endpoint']

        super().__init__(self.clientID, useWebsocket=True)
        super().configureEndpoint(self.endpoint, self.port)
        super().configureCredentials('root-CA.pem')
        self.configure()
        self.connected = super().connect()

        self.base_url = 'http://10.112.59.21:8080/v1.0.0/'

    def configure(self, offline_publish_queueing=-1, draining_frequency=2, connect_disconnect_timeout=10, MQTTOperation_timeout=5):
        '''
        Function to override AWS connection parameters. Cannot be called when the client is currently running.

        :param offline_publish_queueing: number of elements stored in cache if client goes offline, -1 for infinite queueing
        :param draining_frequency: Draining speed (Hz) to clear up the queued requests when the connection is back
        :param connect_disconnect_timeout: the ammount of time in seconds the client will listen for a connection before disconnecting
        :param MQTTOperation_timeout: How long the client should try to complete the MQTT operation before timing out, default is 5 seconds
        '''
        super().configureOfflinePublishQueueing(offline_publish_queueing)
        super().configureDrainingFrequency(draining_frequency)
        super().configureConnectDisconnectTimeout(connect_disconnect_timeout)
        super().configureMQTTOperationTimeout(MQTTOperation_timeout)

    def get_connection_status(self):
        return self.connected

    def get_endpoint(self):
        return self.endpoint

    def get_clientID(self):
        return self.clientID

    def publish(self, topic, payload=None, QoS=1):
        '''
        Publish a message to desired topic via MQTT

        :param topic: Where to publish the message
        :param payload: Custom payload, usually json format
        :param QoS: The quality of service, either 1 or 0
        '''
        super().publish(topic, payload, QoS)
        print('Published to topic: {}'.format(topic))

    def subscribe(self, topic):
        '''
        Listen for incomming messages on desired topic

        :param topic: the topic to listen to
        '''
        print('Subscribed to topic: {}'.format(topic))
        while True:
            super().subscribe(topic, 1, self.__dummy_message)

    def call_agv_to_location(self, mission_id):
        # TODO: handle MIR mission queue?
        # TODO: move this one out of MQTT handler
        status = requests.post(self.base_url + 'mission_queue', mission_id)
        if status.status_code is not 200:
            pprint(status.text)

    def signal(self, signal):
        '''
        Client timeout is as of today locked to 1200 seconds, this will cause problems for a longer running test.
        The trick here is to send a wakeup signal to the MQTT client so that the connection can be reestablished.
        No data will be lost as caching is enabled by default

        :param signal: Enum representing the signal recieved
        '''
        # TODO: actually handle wakeup signal. Finish the signal handling
        if signal is Signal.SIG_AWAKEN.value: # awaken MQTT client
            super().connect()
        elif signal is Signal.SIG_SLEEP.value: # hibernate MQTT client
            pass
        elif signal is Signal.SIG_DISCONNECT.value: # force disconnect MQTT client
            super().disconnect()
        else:
            print('Invalid signal recieved, ignoring')

    def __repr__(self):
        '''
        Function representing the MQTT object in a human-readable format

        :return: description of MQTT object
        '''
        return 'Endpoint: {}\nClientID: {}\nPort: {}'.format(self.endpoint, self.clientID, self.port)

    def __on_message(self, client, data, message):
        payload_json = self.__parser(message)
        pprint(payload_json)
        if 'mission_id' not in payload_json:
            dummy_mission = {'location' : 'some_mission_id'}
            dummy_mission_json = json.dumps(dummy_mission)
            self.call_agv_to_location(dummy_mission_json)
        else:
            self.call_agv_to_location(payload_json['location'])

    def __dummy_message(self, client, data, message):
        # print(client) # None?
        # print(data) # None?
        payload_json = self.__parser(message)
        if 'cake' in payload_json:
            pprint(payload_json)

    def __parser(self, message):
        '''
        Convert MQTTMessage object to Json object

        :param message: the MQTTMessage object recieved from Sub

        :return: Message as Json object
        '''
        decoded = message.payload.decode('utf8')
        return json.loads(decoded)

# just for standalone reasons, do not actually use this in production
if __name__ == "__main__":
    client = MQTTProtocolHandler('customClientID')
    client.get_connection_status()
    client.signal(300)
    client.get_connection_status()
    client.signal(100)
    client.get_connection_status()